#!/usr/bin/python

import glob
import os

TARGET_STRING='<!-- REPLACE -->'

videos = {
  '1_2_wake.txt': 'Rain_Fire.m4v',
  '1_3_to_my_love.txt': 'de_focus_bokeh.mp4',
  '1_4_touch_the_sky.txt': 'clouds.m4v',
  '1_5_thousand_times.txt': 'thousand2.mp4',
  '1_6_navigator.txt': 'navigator2.mp4',
  '1_7_expectation.txt': 'summer_flower.mp4',
  '2_01_all_for_you.txt':'hearts_v2.m4v',
  '2_02_rainy_street.txt':'rain_window.m4v',
  '2_03_start.txt':'Particle_Wave_4K_Motion_Background_Loop.mp4',
  '2_04_sigh.txt':'Snowy_Flower_4K_Living_Background.mp4',
  '2_05_wind_song.txt':'Nature-10287.mp4',
  '2_06_shout_myself.txt':'Nature-10287.mp4',
  '2_07_1_light.txt':'bokeh1.mp4',
  '2_07_2_couple.txt':'bokeh1.mp4',
  '2_08_1_im_your_girl.txt':'Distant_Lights_4K_Motion_Background_Loop.mp4',
  '2_08_2_festival.txt':'Deep_Blue_Particle_Flow_Up.mp4',
  '2_08_3_tell_me.txt':'free-loops_Disco_Led_Lights_19_H264.mp4',
  '2_08_4_fire.txt':'bokeh1.mp4',
  '2_08_5_decalcomani.txt':'bokeh1.mp4',
  '2_09_more_than_flower.txt':'bokeh1.mp4',
  '2_10_to_you.txt':'hd0933.m4v',
  '2_11_family_portrait.txt':'bokeh1.mp4'
}

if __name__=="__main__":
  # read template
  fn_template='../index.html'
  with open(fn_template) as f:
    template = f.read()
  
  files = glob.glob("../lyrics/*.txt")
  for file in files:
    print "Processing ", file
    body = ''
    with open(file) as f:
      if os.path.basename(file) in videos:
        SECTION_ATTRIBUTES='''data-background-video="videos/{}" data-background-video-loop data-background-video-muted
          data-transition="zoom-in fade-out"'''.format(videos[os.path.basename(file)])
      else:
        SECTION_ATTRIBUTES=''
      SECTION='<section {}>\n'.format(SECTION_ATTRIBUTES)
      body += SECTION 
      body += '</section>\n'
      body += SECTION 
      just_opened_section = True
      open_section = True
      for line in f.readlines():
        if len(line.strip()) < 1:
          if not open_section:
            body += SECTION
            open_section = True
            just_opened_section = True
          else:
            body += '</section>\n'
            body += SECTION
            open_section = True
            just_opened_section = True
        else:
          if just_opened_section:
            body += '<p class="fade-in textbg">'+line.strip()+'</p>\n'
          else:
            body += '<p class="fade-in textbg">'+line.strip()+'</p>\n'
          just_opened_section = False
      if open_section:
        body +=  '</section>'

    html_fn = os.path.join('..', os.path.basename(file.replace('.txt', '.html')))
    with open(html_fn, 'w') as f:
      f.write(template.replace(TARGET_STRING, body))